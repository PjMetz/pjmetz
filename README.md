# PjMetz

Education Evangelist on the Education Team

Interested in learning more about free licenses for learning or research? Check out the [GitLab for Education](https://about.gitlab.com/solutions/education/) page!

I make Twitter bots, tutorials, and focus on beginner content. 


Want a Readme for your profile page? [Click here!](https://docs.gitlab.com/ee/user/profile/#add-details-to-your-profile-with-a-readme)
